let purgedPosts = {};
let seenMessages = {};

function normalizeStr(str) { 
    str = str.replace(/[>0-9\s]/g, ''); 
    let result = "";
    let prev = null;
    for(let i = 0; i < str.length; ++i) {
        if(str[i] !== prev)
            result += str[i];
        prev = str[i];
    }
    return result;
}


function _min(d0, d1, d2, bx, ay) {
    return (d0 < d1 || d2 < d1
        ? d0 > d2
            ? d2 + 1
            : d0 + 1
        : bx === ay
            ? d1
            : d1 + 1);
}

function lev(a, b) {
    if(a === b)
        return 0;

    if(a.length > b.length) {
        var tmp = a;
        a = b;
        b = tmp;
    }

    let la = a.length;
    let lb = b.length;

    while(la > 0 && (a.charCodeAt(la - 1) === b.charCodeAt(lb - 1))) {
        la--;
        lb--;
    }

    let offset = 0;

    while(offset < la && (a.charCodeAt(offset) === b.charCodeAt(offset)))
        offset++;

    la -= offset;
    lb -= offset;

    if(la === 0 || lb < 3)
        return lb;

    let x = 0;
    let y;
    let d0;
    let d1;
    let d2;
    let d3;
    let dd;
    let dy;
    let ay;
    let bx0;
    let bx1;
    let bx2;
    let bx3;

    let vector = [];

    for (y = 0; y < la; y++) {
        vector.push(y + 1);
        vector.push(a.charCodeAt(offset + y));
    }

    let len = vector.length - 1;

    for (; x < lb - 3;) {
        bx0 = b.charCodeAt(offset + (d0 = x));
        bx1 = b.charCodeAt(offset + (d1 = x + 1));
        bx2 = b.charCodeAt(offset + (d2 = x + 2));
        bx3 = b.charCodeAt(offset + (d3 = x + 3));
        dd = (x += 4);
        for (y = 0; y < len; y += 2) {
          dy = vector[y];
          ay = vector[y + 1];
          d0 = _min(dy, d0, d1, bx0, ay);
          d1 = _min(d0, d1, d2, bx1, ay);
          d2 = _min(d1, d2, d3, bx2, ay);
          dd = _min(d2, d3, dd, bx3, ay);
          vector[y] = dd;
          d3 = d2;
          d2 = d1;
          d1 = d0;
          d0 = dy;
        }
    }

    for (; x < lb;) {
        bx0 = b.charCodeAt(offset + (d0 = x));
        dd = ++x;
        for (y = 0; y < len; y += 2) {
            dy = vector[y];
            vector[y] = dd = _min(dy, d0, dd, bx0, vector[y + 1]);
            d0 = dy;
        }
    }

    return dd;
};


function purgePony() {
	
	let containerStr = '.replyContainer';
	let replyStr = "a[title='Reply to this post']";
	let postMessageStr = '.postMessage';
	let fileStr = '.file';
	let quotelinkStr = "a.quotelink";
	
	if (window.location.hostname === "boards.4chan.org") {
		if (document.documentElement.className.includes("fourchan-x")) {
			quotelinkStr = "a.backlink";
		}
	}
	else if (window.location.hostname === "desuarchive.org") {
		containerStr = '.post';
		replyStr = "a[title='Reply to this post']";
		postMessageStr = '.text';
		fileStr = '.post_file';
		quotelinkStr = "a.backlink";
	}
	
	document.querySelectorAll(containerStr).forEach(function(container) {
		const postIdC = container.querySelector(replyStr);
		if (!postIdC) {
			return;
		}
		const postId = +postIdC.textContent;
		const messageText = container.querySelector(postMessageStr);
		if (!messageText) {
			return;
		}
		const normalized = normalizeStr(messageText.textContent);
		if (normalized.length === 0) {
			return;
		}
		let min_lev = Number.POSITIVE_INFINITY;
		if (container.querySelector(fileStr)) {
			Object.keys(seenMessages).forEach(function(key) {
				if (key.length <= normalized.length && key.length >= normalized.length / 2 && seenMessages[key] !== postId) {
					let l = lev(key, normalized);
					min_lev = Math.min(min_lev, l);
				}
			});
		}
		if (min_lev <= Math.min(5, normalized.length / 2)) {
			container.style.display = 'none';
			purgedPosts[postId] = true;
		}
		seenMessages[normalized] = postId;
	});
	document.querySelectorAll(containerStr).forEach(function(container) {
		container.querySelectorAll(quotelinkStr).forEach(function(quote) {
			const quoteId = +quote.textContent.replace(/[^0-9.]/g, '');
			if (purgedPosts.hasOwnProperty(quoteId)) {
				quote.style.display = 'none';
			}
		});
	});
}

purgePony();

//const thread = document.querySelector('.thread');
//if(thread) {
//    const config = { attributes: true, childList: true, subtree: true };
//    const observer = new MutationObserver(purgePony);
//    observer.observe(thread, config);
//}
